#!/bin/bash

docker-compose up -d --build

declare landing_page=""

while [ -z "${landing_page}" ]
do
  echo 'Waiting For Spring App To Come Up..'
  landing_page=`docker logs example-project_my_spring_app_1 | grep http://localhost:8080`
  sleep 3
done

echo '=============================================='
echo 'To View Project visit http://localhost:8080'
echo '=============================================='
