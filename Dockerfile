FROM maven

ADD . /project

CMD cd /project && mvn spring-boot:run
