<h1>Welcome To My Example Project In Eclipse</h1>
<h4>Author: Hasan Abu-Rayyan</h4>

<p>
In order to run the project in eclipse. Right Click the project and choose run configuration.<br>
Next create a Maven Build Run with a Goal of: 'spring-boot:run'<br><br>
Then You Can Run That Build and travel to <a href="http://localhost:8080">http://localhost:8080</a> in a browser
</p>