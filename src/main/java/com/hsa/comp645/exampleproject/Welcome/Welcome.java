package com.hsa.comp645.exampleproject.Welcome;

public class Welcome {
    private String name;
    final String message = "Welcome To My Demo, ";

    public Welcome() {
        this.setName("Stranger");
    }

    public Welcome(String name) {
        this.setName(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String greet() {
        return String.format("<h1>%s</h1><p><a href='/'>Go Back</a></p>", this.message + this.name);
    }
}
