package com.hsa.comp645.exampleproject.Welcome;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {
    @RequestMapping("/welcome")
    public String index(@RequestParam(name="name", required = false) String name, Model model) {
        Welcome welcome;
        welcome = name == "" ? new Welcome() : new Welcome(name);
        return welcome.greet();
    }
}
