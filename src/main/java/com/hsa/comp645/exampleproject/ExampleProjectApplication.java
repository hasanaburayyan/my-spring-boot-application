package com.hsa.comp645.exampleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Controller
public class ExampleProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleProjectApplication.class, args);
		System.out.println("To view application please visit http://localhost:8080/");
	}

	@RequestMapping("/")
	public String index() {
		return "/index.html";
	}
}
