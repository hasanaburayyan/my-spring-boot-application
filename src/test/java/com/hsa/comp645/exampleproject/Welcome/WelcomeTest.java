package com.hsa.comp645.exampleproject.Welcome;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WelcomeTest {
    Welcome welcome;

    @Before
    public void setup() {
        // Add any setup here For this test I dont have any.
    }

    @Test
    public void testWelcomeWithName() {
        String name = "Test_name";
        welcome = new Welcome(name);

        String expectedGreeting = formatGreeting(name);
        assertEquals(welcome.greet(), expectedGreeting);
    }

    @Test
    public void testWelcomeWithoutName() {
        welcome = new Welcome();
        String expectedGreeting = formatGreeting("Stranger");
        assertEquals(welcome.greet(), expectedGreeting);
    }

    @Test
    public void testWelcomeSetName() {
        welcome = new Welcome();

        assertEquals(welcome.getName(), "Stranger");

        welcome.setName("Test_name");

        assertEquals(welcome.getName(), "Test_name");
    }

    private String formatGreeting(String name) {
        return String.format("<h1>%s</h1><p><a href='/'>Go Back</a></p>",welcome.message+name);
    }
}
